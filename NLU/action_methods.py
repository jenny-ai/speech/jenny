from NLU.RasaServer import reply

from assistant.joke import get_joke as _get_joke
#from assistant.restaurants_recommendation import recommend_restaurants
#from assistant.Alarm.add_alarm import add_alarm
#from assistant.estimate_trip_time import estimate_trip_time
#from assistant.morningtext import morning_text_form
#from assistant.send_maps_url_to_app import send_maps_url_to_app
#from assistant.places_recommendation import places_recommendation_with_keyword
#from assistant.events_funs import coming_events
from assistant.morningtext import morning_text_form
#from assistant.restaurants_recommendation import recommend_restaurants
#from assistant.places_recommendation import places_recommendation_with_keyword
#from assistant.query import searchOnWiki
#from assistant.estimate_trip_time import estimate_trip_time

from status import Setting,Hotel

from loguru import logger
import datetime
import gevent
from status import Guest

morningthread = None
def get_food(cmd_source,rasa_message_list):
    food = rasa_message_list["food"]
    res = "I will get a list of {} resturants for you".format(food)
    reply(cmd_source,res)
    #list_of_resturants = recommend_restaurants(food_type = food)
    list_of_resturants = [{"name":"koshary hend" , "location_address" : "cairo"},{"name":"pizza hut" , "location_address" : "giza"}]
    if list_of_resturants == False :
        reply(cmd_source,"i am so sorry i can't get it")
    response = "here are some of {} resutrants ".format(food)
    for resturant in list_of_resturants :
        response = response + resturant["name"] + " in " + resturant["location_address"] + " and "
    reply(cmd_source,response[:-5])

def get_joke(cmd_source,rasa_message_list):
    joke = _get_joke()
    logger.debug(joke)
    #joke = "getting a joke"
    if joke == False :
        reply(cmd_source,"i am sorry i haven't any jokes now")
    reply(cmd_source,joke)
    reply(cmd_source,"Do you want to hear another joke?")


def get_not_joke(cmd_source,rasa_message_list):
    res = "Okay as you like"
    reply(cmd_source,res)

def get_wake(cmd_source,rasa_message_list):
    day = rasa_message_list["day"]
    time = rasa_message_list["time"]
    res = "Do you want me to set you an alarm at {0} {1}, do you confirm that?".format(time, day)
    reply(cmd_source,res)

def get_affirm_wake(cmd_source,rasa_message_list):
    day = rasa_message_list["day"]
    time = rasa_message_list["time"]
    if day == "today":
        curent_date = datetime.datetime.now()
        day = curent_date.strftime("%A")
    if "pm" in time.lower() :
        time = "{}00".format(int(time[:-2]) + 12 )
        if int(time) == 24:
            time = "00"
    else :
        time = "{}00".format(int(time[:-2]))

    #if (add_alarm(time,day, alarm_txt, snoozing)):
    if True :
        res = "i have set you an alarm at {0} {1}".format(time, day)
        reply(cmd_source,res)

def get_deny_wake(cmd_source,rasa_message_list):
    res = "okay"
    reply(cmd_source,res)


def get_music(cmd_source,rasa_message_list):
    music = rasa_message_list["music"]
    res = "I'm sorry , i am still learning, i can't play {} music for you , you can play this music from your mobile application".format(music)
    reply(cmd_source,res)

def TurnOnIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the Tv on")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def TurnOffIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the Tv off")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def increaseVolumeIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the volume up")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def decreaseVolumeIOT(cmd_source,rasa_message_list):
    reply(cmd_source,"Turning the volume down")
    res = "unfortunately you can only control your TV from my application , i hope i can do that in the future"
    reply(cmd_source,res)

def estimateTripTime(cmd_source,rasa_message_list):
    dest = rasa_message_list["dest"]
    #time = estimate_trip_time(destination=dest)
    time = "12h45m"
    hours = time[0:2]
    minutes = time[3:5]
    res = "the trip to {} will takes {} hours and {} minutes ".format(dest,hours,minutes)
    reply(cmd_source,res)
@logger.catch
def morningtext(cmd_source,rasa_message_list):
    global morningthread
    #morning_dict = morning_text_form()
    with Guest() as guest :
        name = guest.name
    curent_date = datetime.datetime.now()
    day = curent_date.strftime("%A")
    date = datetime.date.today().strftime("%B %d, %Y")
    #date = morning_text_dict["date"]
    res = "good morning {} today is {} {}".format(name,day,date)
    reply(cmd_source,res)
    with Setting() as setting:
        morning_sections=[]
        if setting.morning_text_qoute:morning_sections.append('quote')
        if setting.morning_text_mail:morning_sections.append('mails')
        if setting.morning_text_news:
            morning_sections.append('news')
        if not (setting.news_interest_sport or setting.news_interest_business or setting.news_interest_entertainment or setting.news_interest_health or setting.news_interest_science): 
            news_category_list=['general']
        else:
            news_category_list=[]
            if setting.news_interest_sport: news_category_list.append('sport')
            if setting.news_interest_business: news_category_list.append('business')
            if setting.news_interest_entertainment: news_category_list.append('entertainment')
            if setting.news_interest_health: news_category_list.append('health')
            if setting.news_interest_science: news_category_list.append('science')
        if setting.morning_text_weather:morning_sections.append('weather')
        logger.debug("before calling morning text")
        morning_text_dict = morning_text_form(morning_sections,setting.news_location_country.lower(),setting.weather_location_city.lower(),news_category_list,2)
        logger.debug("after calling morning text")
        
        morningthread = gevent.spawn(morning_text_thread,cmd_source,morning_sections,morning_text_dict).join()
        
    
@logger.catch
def morning_text_thread (cmd_source,morning_sections,morning_text_dict):
    logger.debug("enter morning text")
    #with Guest() as guest :
    #    name = guest.name
    #curent_date = datetime.datetime.now()
    #day = curent_date.strftime("%A")
    #date = morning_text_dict["date"]
    #res = "good morning {} today is {} {}".format(name,day,date)
    #reply(cmd_source,res)
    if 'weather' in morning_sections:
        weather_tuple = morning_text_dict['weather']
        res= "todays weather is {0} , the maximum temperature is {1} degree and the minimum is {2}".format(weather_tuple[0],weather_tuple[1],weather_tuple[2])
        reply(cmd_source,res)
    if 'quote' in morning_sections:
        quote_tuple = morning_text_dict['quote']
        res= "{0} said {1}".format(quote_tuple[1],quote_tuple[0])
        reply(cmd_source,res)
    if 'mails' in morning_sections:
        mails_list=morning_text_dict['mails']
        res = "recent unread mails                        "
        reply(cmd_source,res)
    
        for mail_dict in mails_list:
            res = "mail from {} with subject {}".format(mail_dict[Sender],mail_dict[Subject])    
    if 'news' in morning_sections:
        news_list=morning_text_dict['news']
        res = "todays news                              "
        reply(cmd_source,res)
        for news_tuple in news_list:
            res = "{}".format(news_tuple[0])
            reply(cmd_source,res)
            
            

def skipMorning(cmd_source,rasa_message_list):
    res = "skipping"
    reply(cmd_source,res)

def stopMorning(cmd_source,rasa_message_list):
    res = "stopping morning text"
    morningthread.kill()
    reply(cmd_source,res)

def emergency(cmd_source,rasa_message_list):
    res = "calling emergency"
    reply(cmd_source,res)

def getWIFIpass(cmd_source,rasa_message_list):
    res = "getting password"
    reply(cmd_source,res)

def provideLocation(cmd_source,rasa_message_list):
    place_location = rasa_message_list["place_location"]
    #res = send_maps_url_to_app(place_location)
    res = True
    if res and not cmd_source :
        res = "okay .. check your mobile application , i sent the location of {} to you".format(place_location)
        reply(cmd_source,res)
    elif res and cmd_source :
        res = "okay .. here is the location"
        reply(cmd_source,res)
    else :
        res = "i am sorry , i can't reach your mobile , please make sure that you have installed our application"
        reply(cmd_source,res)


def placeRecommendation(cmd_source,rasa_message_list):
    recommended_place = rasa_message_list["recommended_place"]
    res = "I will get a list of {} places for you".format(recommended_place)
    reply(cmd_source,res)
    #list_of_places = places_recommendation_with_keyword(keyword=recommended_place, limit=5)
    list_of_places = [{"name":"place name 1" , "location_address" : "location"}]
    if list_of_places == False :
        reply(cmd_source,"i am so sorry i can't get it")
    response = "here are some recomendations of places "
    for place in list_of_places :
        response = response + place["name"] + " in " + place["location_address"] + " and "
    reply(cmd_source,response[:-5])

def remindEvent(cmd_source,rasa_message_list):
    #list_of_events = coming_events()
    list_of_events=False
    if list_of_events==False :
        reply(cmd_source,"you don't have any events")
    response = "you have"
    for event in list_of_events :
        response = response + event + " and "
    reply(cmd_source,response[:-5])
def skip_remind_event(cmd_source,rasa_message_list):
    res = " skip_remind_event"
    reply(cmd_source,res)
def stop_remind_event(cmd_source,rasa_message_list):
    res = "  stop_remind_event "
    reply(cmd_source,res)

def get_hotel_sounds_playlist(cmd_source,rasa_message_list):
    res = "  get_hotel_sounds_playlist  "
    reply(cmd_source,res)

def skip_hotel_sounds(cmd_source,rasa_message_list):
    res = " skip_hotel_sounds "
    reply(cmd_source,res)

def stop_hotel_sounds(cmd_source,rasa_message_list):
    res = " stop_hotel_sounds "
    reply(cmd_source,res)

def stop_music(cmd_source,rasa_message_list):
    res = " stop_music "
    reply(cmd_source,res)

