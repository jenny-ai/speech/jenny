import wavio
from jenny.communication import send_to_local_server,Data
from gevent import sleep

a = wavio.read("demo1.wav")

send_to_local_server(data_id=Data.VOICE, data=[{
    "sampling_rate": a.rate,
    "sample_width": a.sampwidth,
    "audio_data": a.data.tolist()
        }])
    
#print (decodedstr)
