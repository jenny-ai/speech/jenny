import json
from NLU.action_methods import *
from NLU.RasaServer import sendcommand
from NLU.RasaServer import reply
import wavio
from communication import send_to_local_server,Data
from loguru import logger

name_action_dict = {
    "get_food":get_food,
    "get_joke":get_joke,
    "get_not_joke":get_not_joke,
    "get_wake":get_wake,
    "get_affirm_wake":get_affirm_wake,
    "get_deny_wake":get_deny_wake,
    "get_music":get_music,
    "TurnOnIOT":TurnOnIOT,
    "TurnOffIOT":TurnOffIOT,
    "increaseVolumeIOT":increaseVolumeIOT,
    "decreaseVolumeIOT":decreaseVolumeIOT,
    "estimateTripTime":estimateTripTime,
    "morningtext":morningtext,
    "skipMorning":skipMorning,
    "stopMorning":stopMorning,
    "emergency":emergency,
    "getWIFIpass":getWIFIpass,
    "provideLocation":provideLocation,
    "placeRecommendation":placeRecommendation,
    "remindEvent":remindEvent,
    "skip_remind_event":skip_remind_event,
    "get_hotel_sounds_playlist":get_hotel_sounds_playlist,
    "skip_hotel_sounds":skip_hotel_sounds,
    "stop_hotel_sounds":stop_hotel_sounds,
    "stop_remind_event":stop_remind_event,
    "stop_music":stop_music
}

@logger.catch
def get_rasa_response(message):
    """respose data contains :
        1. utter flag : true if utterance response
        2. Rasa flag : true if the assistant wait for a reply
        3. response msg : utter_message   ..... exists when utter = True
        4. fn : function name to call it
    """
    #message = json.loads(message)
    cmd_source = Data(message["dtype"])
    response_list = message["data"]
    response_list = response_list[0]
    response_json = response_list["text"]
    logger.debug(response_json)
    if response_json == [] :
        pass
    else :
        for res_json in response_json :
            if('{' not in res_json):
                res_json = '{'+res_json+'}'
            res_json = json.loads(res_json)
            
            if res_json["utter"] == "True":

                reply(cmd_source,res_json["response"])
                rasaFlag = res_json["rasaFlag"]
            else :
                logger.debug(res_json["fn"])
                response = name_action_dict[res_json["fn"]](cmd_source,res_json)
                rasaFlag = res_json["rasaFlag"]
        with open("rasa_response.json","w+") as f:
            f.write("{"+'"rasaFlag":"{}"'.format(rasaFlag)+"}")
          
    
def askjenny(wavfile):
    """
    take a command string and the source of this command to get a response
    inputs :
     - cmd source : False, the command comes from a voice source
                  : True , the command comes from the app
     - command : string which contains what the user say

    """
    wavdata = wavio.read(wavfile)
    send_to_local_server(data_id=Data.VOICE, data=[{
        "sampling_rate": wavdata.rate,
        "sample_width": wavdata.sampwidth,
        "audio_data": wavdata.data.tolist()
        }])
    





